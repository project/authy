<?php
/**
 * @file
 * Module that implements two factor authentication using Authy.
 */

/**
 * Implements hook_permission().
 */
function authy_permission() {
  return array(
    'administer authy ids' => array(
      'title' => t('Administer Authy IDs'),
      'description' => t('Administer other users Authy ID. The user will also need the "Administer users" permissions on the User module.'),
      'restrict access' => TRUE,
    ),
    'administer own authy id' => array(
      'title' => t('Administer Own Authy ID'),
      'description' => t('Allow the user to admnister his own Authy ID'),
    ),
  );
}

/**
 * Check if the current user is allowed to edit an account.
 *
 * @param object $account
 *   The user to check if a user can administer.
 *
 * @return bool
 *   Returns TRUE if the user is allowed to edit $account.
 */
function authy_permission_administer_access($account) {
  global $user;

  // First check if the loged in user has permission to edit the user at all.
  if (!user_edit_access($account)) {
    return FALSE;
  }

  // Then check if the user is allowed to edit other users Authy settings.
  // If he can. He can edit his own as well.
  if (user_access('administer authy ids') === TRUE) {
    return TRUE;
  }

  // The last hope is that he at least can edit his own ID.
  if (user_access('administer own authy id') === TRUE && $account->uid === $user->uid) {
    return TRUE;
  }

  // All hope is out.
  return FALSE;
}

/**
 * Check if we can unregister a users Authy ID.
 *
 * @param object $account
 *
 * @return bool
 */
function authy_permission_unregister($user) {
  $authy_id = _authy_get_authy_id($user->uid);

  if ($authy_id === FALSE) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Make sure the user has an Authy ID, and therefore can request alternative token.
 */
function authy_permission_request() {
  global $user;

  if (isset($_SESSION['authy']['form_state']['values']['name'])) {
    $authy_user = user_load_by_name($_SESSION['authy']['form_state']['values']['name']);
    $authy_id = _authy_get_authy_id($authy_user->uid);
  }
  else {
    $authy_id = _authy_get_authy_id($user->uid);
  }

  if($authy_id !== FALSE) {
    return TRUE;
  }

  return authy_permission_administer_access($user);
}

/**
 * Check if an user can use the authy/verify form.
 *
 * This should only be available if we are in the middle of a login process.
 */
function authy_permission_verify() {
  // This will only be here if we are in the middle of a login process.
  if(isset($_SESSION['authy']['form'])) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_menu().
 */
function authy_menu() {
  $items = array();

  $items['authy/verify'] = array(
    'title' => 'Provide one-time password',
    'description' => 'Provide one-time password to complete login.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('authy_form_verify'),
    'access callback' => 'authy_permission_verify',
    'type' => MENU_CALLBACK,
    'file' => 'authy.forms.inc',
  );

  $items['authy/request'] = array(
    'title' => 'Request one-time password',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('authy_form_request'),
    'access callback' => 'authy_permission_request',
    'type' => MENU_CALLBACK,
    'file' => 'authy.forms.inc',
  );

  $items['user/%user/authy'] = array(
    'title' => 'Authy',
    'description' => 'Manage your Authy ID',
    'page callback' => 'authy_page_manage',
    'page arguments' => array(1),
    'access callback'  => 'authy_permission_administer_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'authy.pages.inc',
  );

  $items['user/%user/authy/delete'] = array(
    'title' => 'Unregister Authy ID',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('authy_form_unregister', 1),
    'access callback'  => 'authy_permission_unregister',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'authy.forms.inc',
  );

  $items['admin/config/people/authy'] = array(
    'title' => 'Authy',
    'description' => 'Configuration for the authy module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('authy_form_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'authy.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_menu_site_status_alter();
 */
function authy_menu_site_status_alter(&$menu_site_status, $path) {
  $maint_paths = array(
    'authy/verify',
    'authy/request',
  );

  // Allow access to Authy authentication even if site is in offline mode.
  if ($menu_site_status == MENU_SITE_OFFLINE && in_array($path, $maint_paths)) {
    $menu_site_status = MENU_SITE_ONLINE;
  }
}

/**
 * Implements hook_help().
 */
function authy_help($path, $arg) {
  $output = NULL;

  switch ($path) {
    case 'admin/config/people/authy':
      $output = t('This is where you configure how the Authy module should behave. For details please see the <a href="!help">help</a> page.',
        array(
          '!help' => url('admin/help/authy'),
        )
      );

      break;

    case 'admin/help#authy';
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Authy module provides two-factor authentication with Authy to your Drupal site.') . '</p>';

      $output .= '<h3>' . t('Permissions') . '</h3>';
      $output .= '<p>' . t('The Authy module enables you to give the following permission to users.') . '</p>';
      $output .= '<dl>';

      $output .= '<dt>' . t('Administer Authy IDs') . '</dt>';
      $output .= '<dd>' . t('Allows a user to edit other users Authy settings. applies only to accounts he already has permission to edit.') . '</dd>';

      $output .= '<dt>' . t('Administer Own Authy ID') . '</dt>';
      $output .= '<dd>' . t('Allows a user to edit his own Authy settings.') . '</dd>';

      $output .= '</dl>';

      $output .= '<h3>' . t('Configuration') . '</h3>';
      $output .= '<p>' . t('The are a few settings that will help you tweak how Authy works on your site.') . '</p>';
      $output .= '<dl>';

      $output .= '<dt>' . t('Authy API') . '</dt>';
      $output .= '<dd>' . t('Selects what API the module will use while communicating with Authy. Should normaly be the Production API.') . '</dd>';

      $output .= '<dt>' . t('Authy Application name') . '</dt>';
      $output .= '<dd>' . t('The name of the Authy application. This will help the user to select the correct application in the Authy App.') . '</dd>';

      $output .= '<dt>' . t('API Key') . '</dt>';
      $output .= '<dd>' . t('The API key that belongs to the Authy application.') . '</dd>';

      $output .= '<dt>' . t('Authy enabled forms') . '</dt>';
      $output .= '<dd>' . t('Additional forms that requires Authy verification. If you leave this empty, only logins will require Authy verification.') . '</dd>';

      $output .= '</dl>';

      break;
  }

  return $output;
}

/**
 * Implements hook_form_alter().
 *
 * Alter the login form and add an extra validator to check
 * if the user need to provide a Authy token.
 */
function authy_form_alter(&$form, $form_state, $form_id) {
  global $user;

  if(variable_get('authy_show_form_id', 'n') == 'y') {
    drupal_set_message(t('Form ID: %form', array('%form' => $form_id)), 'status', FALSE);
  }

  $forms = variable_get('authy_forms', '');
  $forms = explode("\r\n", $forms);

  $login_forms = array(
    'user_login',
    'user_login_block',
  );

  if (in_array($form_id, $login_forms)) {
    /* Make sure everything works before adding Authy fields to a form. */
    $authy = _authy_get_api();
    if ($authy === FALSE) {
      return;
    }
    // If we are logging in, replace the default Drupal user_login_submit()
    // with our own authy_login_submit().
    $form['#submit'] = array('authy_login_submit');
  }
  elseif (in_array($form_id, $forms)) {
    /* Make sure everything works before adding Authy fields to a form. */
    $authy = _authy_get_api();
    if ($authy === FALSE) {
      return;
    }
    // Normal form with Authy required.
    if ($user->uid > 0) {
      /* Get Authy ID for the current user. */
      $authy_id = _authy_get_authy_id($user->uid);
      /* Only require Authy if user has it enabled. */
      if ($authy_id === FALSE) {
        return;
      }
    }
    else {
      /* Don't do anything for anonymous users. */
      return;
    }

    // Add the Authy validator to the form.
    $form['#validate'][] = 'authy_verify';

    $form['#attached']['css'] = array(
      'https://www.authy.com/form.authy.min.css' => array(
        'type' => 'external',
      ),
    );

    // Add the Authy input field to the form.
    $form['authy'] = array(
      '#type' => 'fieldset',
      '#title' => t('Confirm your identity'),
      '#description' => t('Please provide us with your %application Authy token to complete this action.', array('%application' => variable_get('authy_application', ''))),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#weight' => 99,
    );

    $form['authy']['authy_token'] = array(
      '#type' => 'textfield',
      '#title' => t('Authy Token'),
      '#attributes' => array('id' => array('authy-token')),
      '#weight' => 100,
      '#description' => t('Enter your Authy token for %application. <a href="!request">Request SMS token</a>.', array(
        '!request' => url('authy/request'),
        '%application' => variable_get('authy_application', ''),
      )),
      '#required' => TRUE,
    );
  }
}

/**
 * Form submit callback for login forms.
 *
 * Checks to see if a user has Authy enabled, and
 * redirects to the Authy verification form.
 */
function authy_login_submit($form, &$form_state) {

  // First we load the user that is trying to login to see if Authy is enabled.
  $authy_user = user_load_by_name($form_state['values']['name']);
  $authy_id = _authy_get_authy_id($authy_user->uid);

  if ($authy_id === FALSE) {
    // If the user don't have Authy enabled, just log in the normal way.
    user_login_submit($form, $form_state);
  }
  else {
    // We need to stop drupal_goto() from stealing our redirect, but we will
    // take care ofthe redirect ourself, later.
    if (isset($_GET['destination'])) {
      $_SESSION['authy']['destination'] = $_GET['destination'];
      unset($_GET['destination']);
    }

    // Save the form state in the user session.
    // We need this to complete the login later.
    $_SESSION['authy']['form_state'] = $form_state;
    $_SESSION['authy']['form'] = $form;

    // Redirect the user and ask for the Authy token.
    drupal_set_message(t('Please provide your Authy token to continue.'));
    $form_state['redirect'] = 'authy/verify';
  }
}

/**
 * Common function for verification of Authy token.
 *
 * Basically it implements hook_form_validate(), and is added to
 * any form by hook_form_alter(), but also called dicrectly by
 * authy_form_verify_validate().
 */
function authy_verify($form, &$form_state) {
  global $user;

  // Only require token if we are submitting a form. Thanks techypaul!
  if (!$form_state['submitted']) {
    return TRUE;
  }
  // If no user is logged in, this is probably an login or password reset
  // attempt. In that case we will have a name value with username or email.
  if (user_is_anonymous() === TRUE && isset($form_state['values']['name'])) {
    // Most likely login attempt. Try to load from username.
    $authy_user = user_load_by_name($form_state['values']['name']);

    if ($authy_user === FALSE) {
      // No user found, try by e-mail.
      $authy_user = user_load_by_mail($form_state['values']['name']);
    }

    if ($authy_user === FALSE) {
      // No user found. We don't need Authy verification.
      return TRUE;
    }
    else {
      // We got the user that is trying to authenticate. Get uid.
      $uid = $authy_user->uid;
    }
  }
  else {
    // We are authenticated. Get uid from $user object.
    $uid = $user->uid;
  }

  $authy_id = _authy_get_authy_id($uid);

  if ($authy_id === FALSE) {
    // User don't have Authy configured.
    return TRUE;
  }

  if (trim($form_state['values']['authy_token']) == '') {
    form_set_error('authy_token', t('Authy Token field is required.'));
  }

  $authy = _authy_get_api();
  $authy->_serverTimeout = 30;
  $verification = $authy->verify($authy_id, $form_state['values']['authy_token']);

  if ($verification === TRUE) {
    // Zero the counter that counts number of failed attempts.
    unset($_SESSION['authy']['failed']);

    return TRUE;
  }
  else {
    // Invalid token entered. Write a watchdog message.
  	if (user_is_anonymous() === TRUE) {
  	  // Anonymous user. This is a login or password reset attempt.
  	  watchdog('Authy', 'Authy authentication failed for @user.', array('@user' => format_username($authy_user)), WATCHDOG_WARNING);

      // Count the number of failed logins.
      if (!isset($_SESSION['authy']['failed'])) {
        $_SESSION['authy']['failed'] = 0;
      }
      $_SESSION['authy']['failed']++;

      // Trigger either multiple or single trigger.
      $threshold = variable_get('authy_trigger_threshold', '5');
      if ($threshold <= $_SESSION['authy']['failed']) {

        module_invoke('rules', 'invoke_event', 'authy_failed_multiple', $authy_user);
        // Fine, we did something. Now start counting again.
        $_SESSION['authy']['failed'] = 0;
      }
      else {
        // Trigger the single failed login... trigger.
        module_invoke('rules', 'invoke_event', 'authy_failed', $authy_user);
      }

  	}
  	else {
  	  // User is logged in. This is a form that requires Authy.
  	  watchdog('Authy', 'Invalid token entered in form.', array(), WATCHDOG_WARNING);
  	}
    watchdog('Authy', 'Authentication failed: %error', array('%error' => $authy->lastError), WATCHDOG_DEBUG);
    form_set_error('authy_token', t('Sorry, but the token you entered could not be validated.'));
  }
}

/**
 * Implements hook_rules_event_info().
 */
function authy_rules_event_info() {
  $defaults = array(
    'group' => t('Authy'),
    'module' => 'authy',
  );
  return array(
    'authy_login' => $defaults + array(
      'label' => t('Authy authentication successful'),
      'variables' => array(
        'user' => array('type' => 'user', 'label' => t('User information')),
      ),
    ),
    'authy_failed' => $defaults + array(
      'label' => t('Authy authentication failed once'),
      'variables' => array(
        'user' => array('type' => 'user', 'label' => t('User information')),
      ),
    ),
    'authy_failed_multiple' => $defaults + array(
      'label' => t('Authy authentication failed !times times', array('!times' => variable_get('authy_trigger_threshold', '5'))),
      'variables' => array(
        'user' => array('type' => 'user', 'label' => t('User information')),
      ),
    ),
    'authy_enabled' => $defaults + array(
      'label' => t('User enabled Authy'),
      'variables' => array(
        'user' => array('type' => 'user', 'label' => t('User identifier')),
      ),
    ),
    'authy_disabled' => $defaults + array(
       'label' => t('User disabled Authy'),
      'variables' => array(
        'user' => array('type' => 'user', 'label' => t('User identifier')),
      ),
    ),
  );
}

/**
 * Get Authy ID for a user.
 *
 * @param int $uid
 *   User ID to get Authy ID for.
 *
 * @param boolean $fetch_inactive
 *   Set this to TRUE to also fetch Authy ID if it's not active.
 *
 * @return int
 *   Returns a users Authy ID or false if no ID is found.
 */
function _authy_get_authy_id($uid, $fetch_inactive = FALSE) {
  if  ($fetch_inactive === TRUE) {
    $result = db_query("SELECT authy_id FROM {authy} WHERE uid = :uid", array(
      ':uid'    => $uid,
    ));
  }
  else {
    $result = db_query("SELECT authy_id FROM {authy} WHERE uid = :uid AND active = :active", array(
      ':uid'    => $uid,
      ':active' => 1,
    ));
  }



  $record = $result->fetchAssoc();

  if (isset($record['authy_id'])) {
    return $record['authy_id'];
  }

  return FALSE;
}

/**
 * Get Authy API.
 *
 * Try to load the authy-php library, and check configuration.
 *
 * @return mixed
 *   Returns authy-php object on success or FALSE if configuration
 *   or library is missing.
 */
function _authy_get_api() {
  $key = variable_get('authy_api_key', '');
  $host = variable_get('authy_host_uri', 'https://api.authy.com');

  if (strlen($key) !== 32) {
    watchdog('Authy', 'No API key configured.', array(), WATCHDOG_ALERT);
    return FALSE;
  }

  module_load_include('php', 'authy', 'lib/Authy');

  $authy_api = new Authy();
  $authy_api->_server = $host;
  $authy_api->_apiKey = $key;

  return $authy_api;
}

